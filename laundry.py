from osv import osv
from osv import fields

class laundry_partner(osv.osv):
 
    _name = 'res.partner'
    _inherit = 'res.partner'
    _columns = {
            'phone': fields.related('address', 'phone', type='char', string='Phone', select=True),
        }
laundry_partner()

class laundry_order(osv.osv):
 
    _name = 'sale.order'
    _description = 'Sale Order'
    _inherit = 'sale.order'
 
    _columns = {
            'laundry':fields.boolean('Laundry'),   
        }
    
    
laundry_order()

class laundry_product(osv.osv):
 
    _name = 'product.product'
    _inherit = 'product.product'
 
    _columns = {
            'laundry':fields.boolean('Laundry'),   
        }
    
    
laundry_product()

class laundry_order_line(osv.osv):
 
    _name = 'sale.order.line'
    _description = 'Sale Order Lines'
    _inherit = 'sale.order.line'
 
    _columns = {
            'label_value_ids':fields.one2many('laundry.line.label','line_id','Label'),   
        }
    
    def button_confirm(self, cr, uid, ids, context=None):
        so_line = self.browse(cr,uid,ids[0])
        print "SO",so_line.order_id.name
        if so_line.order_id.laundry == 1:
            self.pool.get('project.task').create(cr, uid,
                                             {
                                              'name':so_line.order_id.name,
                                              'date_start':so_line.order_id.date_confirm,
                                              'partner_id':so_line.order_id.partner_id.id,
                                              'user_id':so_line.order_id.user_id.id,
                                              'date_deadline':so_line.order_id.commitment_date,
                                              }                                   
                                            )
        return self.write(cr, uid, ids, {'state': 'confirmed'})
    
laundry_order_line()

class laundry_line_label(osv.osv):
 
    _name = 'laundry.line.label'
    _description = 'laundry.line.label'
 
    _columns = {
            'name':fields.char('Code', size=64, required=True),
            'qty':fields.integer('Qty'),
            'label_ids':fields.many2many('laundry.label','laundry_line_label_rel','label_line_id','label_id','Labels',required=True),
            'line_id':fields.many2one('sale.order.line','Order Line',required=True),
        }
laundry_line_label()

class laundry_label(osv.osv):
 
    _name = 'laundry.label'
    _description = 'laundry.label'
 
    _columns = {
            'name':fields.char('Name', size=64, required=True),
            'seq':fields.integer('Seq')
        }
laundry_label()